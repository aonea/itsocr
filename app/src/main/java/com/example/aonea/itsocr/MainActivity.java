package com.example.aonea.itsocr;

import android.Manifest;
import android.annotation.SuppressLint;
import android.content.pm.PackageManager;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.graphics.Canvas;
import android.graphics.Color;
import android.graphics.Paint;
import android.graphics.drawable.BitmapDrawable;
import android.graphics.drawable.ShapeDrawable;
import android.graphics.drawable.shapes.RectShape;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v4.app.ActivityCompat;
import android.support.v4.content.ContextCompat;
import android.support.v7.app.AppCompatActivity;
import android.util.Log;
import android.view.MotionEvent;
import android.view.View;
import android.view.ViewGroup;
import android.view.WindowManager;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.Toast;

import com.wonderkiln.camerakit.CameraKitError;
import com.wonderkiln.camerakit.CameraKitEvent;
import com.wonderkiln.camerakit.CameraKitEventListener;
import com.wonderkiln.camerakit.CameraKitImage;
import com.wonderkiln.camerakit.CameraKitVideo;
import com.wonderkiln.camerakit.CameraView;

import java.io.IOException;
import java.util.List;
import java.util.Timer;
import java.util.TimerTask;

public class MainActivity extends AppCompatActivity {
    //TODO: draw rectangle dinamically on camera view

    private static final String TF_OD_API_MODEL_FILE = "file:///android_asset/frozen_inference_graph.pb";
    private static final String TF_OD_API_LABELS_FILE = "file:///android_asset/labels.txt";
    private static final int INPUT_SIZE = 1080;

    private LinearLayout linearLayout;
    private CameraView cameraView;
    private ImageView ivShutter;
    private ImageView ivRectangle;
    private ImageView ivRecognized;

    private Classifier detector;

    private ShapeDrawable rect = null;

    @Override
    protected void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);

        getWindow().setFlags(WindowManager.LayoutParams.FLAG_FULLSCREEN,
                WindowManager.LayoutParams.FLAG_FULLSCREEN);

        try {
            detector = TensorFlowObjectDetectionAPIModel.create(
                    getAssets(), TF_OD_API_MODEL_FILE, TF_OD_API_LABELS_FILE, INPUT_SIZE
            );
        } catch (IOException e) {
            e.printStackTrace();
        }

        initViews();
        checkPermissions();
        setListeners();
    }

    @Override
    protected void onResume() {
        super.onResume();
        cameraView.start();
    }

    @Override
    protected void onPause() {
        cameraView.stop();
        super.onPause();
    }

    private void initViews() {
        linearLayout = findViewById(R.id.linear_layout);
        cameraView = findViewById(R.id.camera_view);
//        ivShutter = findViewById(R.id.ivShutter);
        ivRectangle = findViewById(R.id.ivRectangle);
    }

    private void checkPermissions() {
        if (ContextCompat.checkSelfPermission(
                this, Manifest.permission.CAMERA) != PackageManager.PERMISSION_GRANTED
                ) {
            ActivityCompat.requestPermissions(
                    this, new String[]{Manifest.permission.CAMERA, Manifest.permission.WRITE_EXTERNAL_STORAGE}, 1);
        }
    }

    @SuppressLint("ClickableViewAccessibility")
    private void setListeners() {
        cameraView.addCameraKitListener(new CameraKitEventListener() {
            @Override
            public void onEvent(CameraKitEvent cameraKitEvent) {
                Log.i("Event", cameraKitEvent.toString());
            }

            @Override
            public void onError(CameraKitError cameraKitError) {

            }

            @Override
            public void onImage(CameraKitImage cameraKitImage) {
                Bitmap capture = cameraKitImage.getBitmap();

                Bitmap cropped = getCroppedBitmap(
                        capture,
                        (int) ivRectangle.getX(),
                        (int) ivRectangle.getY(),
                        ivRectangle.getWidth(),
                        ivRectangle.getHeight()
                );

                List<Classifier.Recognition> results = detector.recognizeImage(cropped);
                String recognitionString =
                        String.valueOf(Math.floor(results.get(0).getConfidence() * 100) / 100);
                Log.i("Recognitions", recognitionString);
                Toast.makeText(MainActivity.this, recognitionString, Toast.LENGTH_SHORT).show();
                drawRectangle((int) results.get(0).getLocation().centerX(),
                        (int) results.get(0).getLocation().centerY(),
                        (int) results.get(0).getLocation().width(),
                        (int) results.get(0).getLocation().height());


                Bitmap recognizedBitmap = getCroppedBitmap(
                        capture,
                        (int) ivRecognized.getX(),
                        (int) ivRecognized.getY(),
                        rect.getIntrinsicWidth(),
                        rect.getIntrinsicHeight());
            }

            @Override
            public void onVideo(CameraKitVideo cameraKitVideo) {

            }
        });

        //Shutter
        //timed
//        Timer timer = new Timer();
//        timer.scheduleAtFixedRate(new TimerTask() {
//            @Override
//            public void run() {
//                cameraView.captureImage();
//            }
//        }, 0, 2000);

        cameraView.setOnTouchListener(new View.OnTouchListener() {
            @Override
            public boolean onTouch(View view, MotionEvent motionEvent) {
                if (motionEvent.getAction() == MotionEvent.ACTION_DOWN) {
                    cameraView.captureImage();
                }
                return false;
            }
        });
    }

    private void drawRectangle(int centerX, int centerY, int width, int height) {
        if (rect == null) {
            rect = new ShapeDrawable(new RectShape());
        }

        rect.setIntrinsicWidth(width);
        rect.setIntrinsicHeight(height);
        rect.getPaint().setStrokeWidth(3);
        rect.getPaint().setStyle(Paint.Style.STROKE);
        rect.getPaint().setColor(Color.RED);

        if (ivRecognized == null) {
            ivRecognized = new ImageView(this);
            ivRecognized.setLayoutParams(new LinearLayout.LayoutParams(
                    ViewGroup.LayoutParams.WRAP_CONTENT,
                    ViewGroup.LayoutParams.WRAP_CONTENT)
            );
            cameraView.addView(ivRecognized);
        }

        ivRecognized.setImageDrawable(rect);
        ivRecognized.setX(centerX - width / 2);
        ivRecognized.setY(centerY - height / 2 + 420);
    }

    private Bitmap screenShotView(View view) {
        Bitmap bitmap = Bitmap.createBitmap(view.getWidth(), view.getHeight(), Bitmap.Config.ARGB_8888);
        Bitmap mutable = bitmap.copy(Bitmap.Config.ARGB_8888, true);
        Canvas canvas = new Canvas(mutable);
        view.draw(canvas);

        return bitmap;
    }

    private Bitmap getCroppedBitmap(Bitmap source, int x, int y, int width, int height) {
        return Bitmap.createBitmap(
                source,
                x,
                y,
                width,
                height
        );
    }

}
