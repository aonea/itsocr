# README #

OCR for internal use featuring TensorFlow and Google VisionAPI

### Tasks ###
* send photos to model seamlessly
* send photo to Vision API and get back text

### Debugging issues ###

* If when trying to debug the app you're getting an 'ERROR_INSUFFICIENT_STORAGE' that suggests you uninstall your app, but the app is already uninstalled, try clearing your phone's cache and try again
* Make sure you change INPUT_SIZE (main activity) and height and width of the rectangle shape (rectangle.xml) to your phone's width (ex. 720px, 1080px, etc.) until we change it dynamically